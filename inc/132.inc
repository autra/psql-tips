    <div class="tip">The <code>\exec</code>
      metacommand will execute the result of the current query or the last query
      if the current query buffer is empty. This is a very useful feature to
      generate DDL and execute it in one go.
      <pre><code class="hljs bash">laetitia=*# select 'drop table ' || table_name 
laetitia-*# from information_schema.tables 
laetitia-*# where table_schema = 'public'
laetitia-*#   and table_name ~ 'test';
     ?column?     
------------------
 drop table test
 drop table test2
 drop table test3
(3 rows)

laetitia=*# \gexec
DROP TABLE
DROP TABLE
DROP TABLE</code></pre>This feature is available since Postgres 9.6.
	</div>
