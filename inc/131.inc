    <div class="tip">The <code>\gdesc</code>
      metacommand will show the decsription (column names and datatypes) of the
      current query buffer. The wuery is not actually executed.
      <pre><code class="hljs bash">laetitia=# select * from test;
 id | value 
----+-------
  1 | bla
  2 | bla
  3 | bla
  4 | bla
  5 | bla
  6 | bla
(6 rows)

laetitia=# \gdesc
 Column |  Type   
--------+---------
 id     | integer
 value  | text
(2 rows)
</code></pre>This feature is available since Postgres 11.
	</div>
